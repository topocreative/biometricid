package sk.topo.biometricid.biometric

import android.annotation.TargetApi
import android.content.DialogInterface
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.CancellationSignal

class BiometricManager(val builder: Biometrics.Builder) : Biometrics {

    override fun authenticate(biometricCallback: BiometricCallback) {
        if (!BiometricUtils.isSdkVersionSupported) {
            biometricCallback.onSdkVersionNotSupported()
        } else if (!BiometricUtils.isPermissionGranted(builder.context)) {
            biometricCallback.onBiometricAuthenticationPermissionNotGranted()
        } else if (!BiometricUtils.isHardwareSupported(builder.context)) {
            biometricCallback.onBiometricAuthenticationNotSupported()
        } else if (!BiometricUtils.isFingerprintAvailable(builder.context)) {
            biometricCallback.onBiometricAuthenticationNotAvailable()
        } else {
            displayBiometricPrompt(biometricCallback)
        }
    }

    @TargetApi(Build.VERSION_CODES.P)
    private fun displayBiometricPrompt(biometricCallback: BiometricCallback) {
        BiometricPrompt.Builder(builder.context)
            .setTitle(builder.title)
            .setSubtitle(builder.subtitle)
            .setDescription(builder.description)
            .setNegativeButton(
                builder.negativeButtonText,
                builder.context.mainExecutor,
                DialogInterface.OnClickListener { _, _ -> biometricCallback.onAuthenticationCancelled() })
            .build()
            .authenticate(
                CancellationSignal(), builder.context.mainExecutor,
                BiometricCallbackV28(biometricCallback)
            )
    }
}
