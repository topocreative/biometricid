package sk.topo.biometricid.biometric

import android.annotation.TargetApi
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.support.v4.os.CancellationSignal
import android.util.Log
import sk.topo.biometricid.R
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


@TargetApi(Build.VERSION_CODES.M)
class BiometricManagerV23(val builder: Biometrics.Builder) : Biometrics {

    private var cipher: Cipher? = null
    private var keyStore: KeyStore? = null
    private var keyGenerator: KeyGenerator? = null
    private var cryptoObject: FingerprintManagerCompat.CryptoObject? = null

    private var biometricDialogV23: BiometricDialogV23? = null

    override fun authenticate(biometricCallback: BiometricCallback) {
        if (!BiometricUtils.isSdkVersionSupported) {
            biometricCallback.onSdkVersionNotSupported()
        } else if (!BiometricUtils.isPermissionGranted(builder.context)) {
            biometricCallback.onBiometricAuthenticationPermissionNotGranted()
        } else if (!BiometricUtils.isHardwareSupported(builder.context)) {
            biometricCallback.onBiometricAuthenticationNotSupported()
        } else if (!BiometricUtils.isFingerprintAvailable(builder.context)) {
            biometricCallback.onBiometricAuthenticationNotAvailable()
        } else {
            displayBiometricDialog(biometricCallback)
        }
    }

    private fun displayBiometricDialog(biometricCallback: BiometricCallback) {
        try {
            if (generateKey() && initCipher()) {

                cryptoObject = FingerprintManagerCompat.CryptoObject(cipher!!)
                val fingerprintManagerCompat = FingerprintManagerCompat.from(builder.context)

                fingerprintManagerCompat.authenticate(
                    cryptoObject, 0, CancellationSignal(),
                    object : FingerprintManagerCompat.AuthenticationCallback() {
                        override fun onAuthenticationError(
                            errMsgId: Int,
                            errString: CharSequence?
                        ) {
                            super.onAuthenticationError(errMsgId, errString)
                            updateStatus(errString.toString())
                            biometricCallback.onAuthenticationError(errMsgId, errString)
                        }

                        override fun onAuthenticationHelp(
                            helpMsgId: Int,
                            helpString: CharSequence?
                        ) {
                            super.onAuthenticationHelp(helpMsgId, helpString)
                            updateStatus(helpString.toString())
                            biometricCallback.onAuthenticationHelp(helpMsgId, helpString)
                        }

                        override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
                            super.onAuthenticationSucceeded(result)
                            dismissDialog()
                            biometricCallback.onAuthenticationSuccessful()
                        }


                        override fun onAuthenticationFailed() {
                            super.onAuthenticationFailed()
                            updateStatus(builder.context.getString(R.string.biometric_failed))
                            biometricCallback.onAuthenticationFailed()
                        }
                    },
                    null
                )

                displayDialog(biometricCallback)
            }
        } catch (e: Exception) {
            Log.e("BiometricV23", e.message, e)
            biometricCallback.onAuthenticationFailed()
        }
    }


    private fun displayDialog(biometricCallback: BiometricCallback) {
        biometricDialogV23 = BiometricDialogV23(builder.context, biometricCallback)
        biometricDialogV23?.apply {
            setTitle(builder.title)
            setDescription(builder.description)
            setSubtitle(builder.subtitle)
            setButtonText(builder.negativeButtonText)
            show()
        }
    }


    private fun dismissDialog() {
        biometricDialogV23?.dismiss()
    }

    private fun updateStatus(status: String) {
        biometricDialogV23?.updateStatus(status)
    }

    private fun generateKey(): Boolean {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore")
            keyStore?.load(null)

            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            keyGenerator?.init(
                KeyGenParameterSpec.Builder(KEY_NAME, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build()
            )

            keyGenerator?.generateKey()
            return true

        } catch (e: Exception) {
            Log.e("BiometricV23", e.message, e)
        }
        return false
    }


    private fun initCipher(): Boolean {
        try {
            cipher = Cipher.getInstance(
                KeyProperties.KEY_ALGORITHM_AES + "/"
                        + KeyProperties.BLOCK_MODE_CBC + "/"
                        + KeyProperties.ENCRYPTION_PADDING_PKCS7
            )

        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get Cipher", e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException("Failed to get Cipher", e)
        }

        try {
            keyStore?.let { keyStore ->
                keyStore.load(null)
                val key = keyStore.getKey(KEY_NAME, null) as SecretKey
                cipher?.init(Cipher.ENCRYPT_MODE, key)
                return true
            }
        } catch (e: KeyPermanentlyInvalidatedException) {
            return false
        } catch (e: Exception) {
            throw RuntimeException("Failed to init Cipher", e)
        }
        return false
    }

    companion object {
        private val KEY_NAME = UUID.randomUUID().toString()
    }
}
