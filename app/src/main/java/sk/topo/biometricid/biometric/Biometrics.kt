package sk.topo.biometricid.biometric

import android.content.Context

interface Biometrics {

    fun authenticate(biometricCallback: BiometricCallback)

    class Builder(
        val context: Context,
        val title: String,
        val subtitle: String,
        val description: String,
        val negativeButtonText: String
    ) {
        fun build(): Biometrics {
            return if (BiometricUtils.isBiometricPromptEnabled) {
                BiometricManager(this)
            } else {
                BiometricManagerV23(this)
            }
        }
    }
}