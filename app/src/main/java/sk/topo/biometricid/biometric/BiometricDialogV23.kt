package sk.topo.biometricid.biometric

import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.view_bottom_sheet.view.*
import sk.topo.biometricid.R

class BiometricDialogV23(context: Context, private val biometricCallback: BiometricCallback) :
    BottomSheetDialog(context, R.style.BottomSheetDialogTheme), View.OnClickListener {


    private lateinit var btnCancel: Button
    private lateinit var imgLogo: ImageView
    private lateinit var itemTitle: TextView
    private lateinit var itemDescription: TextView
    private lateinit var itemSubtitle: TextView
    private lateinit var itemStatus: TextView

    init {
        setDialogView()
    }


    private fun setDialogView() {
        val bottomSheetView = layoutInflater.inflate(R.layout.view_bottom_sheet, null)
        setContentView(bottomSheetView)

        btnCancel = bottomSheetView.btn_cancel
        btnCancel.setOnClickListener(this)

        imgLogo = bottomSheetView.img_logo
        itemTitle = bottomSheetView.item_title
        itemStatus = bottomSheetView.item_status
        itemSubtitle = bottomSheetView.item_subtitle
        itemDescription = bottomSheetView.item_description

        updateLogo()
    }

    fun setTitle(title: String) {
        itemTitle.text = title
    }

    fun updateStatus(status: String) {
        itemStatus.text = status
    }

    fun setSubtitle(subtitle: String) {
        itemSubtitle.text = subtitle
    }

    fun setDescription(description: String) {
        itemDescription.text = description
    }

    fun setButtonText(negativeButtonText: String) {
        btnCancel.text = negativeButtonText
    }

    private fun updateLogo() {
        try {
            val drawable = context.packageManager.getApplicationIcon(context.packageName)
            imgLogo.setImageDrawable(drawable)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onClick(view: View) {
        dismiss()
        biometricCallback.onAuthenticationCancelled()
    }
}


