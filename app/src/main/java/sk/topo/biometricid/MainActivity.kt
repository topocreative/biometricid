package sk.topo.biometricid

import android.content.Context
import android.hardware.biometrics.BiometricPrompt
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import sk.topo.biometricid.biometric.BiometricCallback
import sk.topo.biometricid.biometric.Biometrics


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            showBiometric()
        }
    }

    private val biometricTitleText by lazy { getString(R.string.biometric_dialog_title) }
    private val biometricSubtitleText by lazy { getString(R.string.biometric_dialog_subtitle) }
    private val biometricDescText by lazy { getString(R.string.biometric_dialog_desc) }
    private val biometricButtonCancelText by lazy { getString(R.string.biometric_dialog_cancel) }

    private fun showBiometric() {
        Biometrics.Builder(
            this,
            biometricTitleText,
            biometricSubtitleText,
            biometricDescText,
            biometricButtonCancelText
        )
            .build()
            .authenticate(provideBiometricCallBack(this))
    }

    private fun provideBiometricCallBack(applicationContext: Context): BiometricCallback {
        return object : BiometricCallback {

            override fun onSdkVersionNotSupported() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_error_sdk_not_supported),
                    Toast.LENGTH_LONG
                )
                    .show()
                biometricFailed()
            }

            override fun onBiometricAuthenticationNotSupported() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_error_hardware_not_supported),
                    Toast.LENGTH_LONG
                ).show()
                biometricFailed()
            }

            override fun onBiometricAuthenticationNotAvailable() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_error_fingerprint_not_available),
                    Toast.LENGTH_LONG
                ).show()
                biometricFailed()
            }

            override fun onBiometricAuthenticationPermissionNotGranted() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_error_permission_not_granted),
                    Toast.LENGTH_LONG
                ).show()
                biometricFailed()
            }

            override fun onBiometricAuthenticationInternalError(error: String) {
                Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                biometricFailed()
            }

            override fun onAuthenticationFailed() {
                Toast.makeText(
                    getApplicationContext(),
                    getString(R.string.biometric_failure),
                    Toast.LENGTH_LONG
                ).show()
                biometricFailed()
            }

            override fun onAuthenticationCancelled() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_cancelled),
                    Toast.LENGTH_LONG
                ).show()
                biometricFailed()
            }

            override fun onAuthenticationSuccessful() {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.biometric_success),
                    Toast.LENGTH_LONG
                ).show()
                biometricSuccess()
            }

            override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
                Toast.makeText(getApplicationContext(), helpString, Toast.LENGTH_LONG).show()
            }

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_LONG).show()
                when (errorCode) {
                    BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT,
                    BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT_PERMANENT,
                    BiometricPrompt.BIOMETRIC_ERROR_TIMEOUT,
                    BiometricPrompt.BIOMETRIC_ERROR_VENDOR -> {
                        biometricFailed()
                    }
                }
            }
        }
    }

    fun biometricFailed() {
        //do something, show classic login
    }

    fun biometricSuccess() {
        //go to app content
    }
}
